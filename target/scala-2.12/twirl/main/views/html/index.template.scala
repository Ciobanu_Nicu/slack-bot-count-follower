
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[String,Option[User],List[User],play.twirl.api.HtmlFormat.Appendable] {

  /*
 * This template takes a single argument, a String containing a
 * message to display.
 */
  def apply/*5.2*/(message: String)(user: Option[User])(users: List[User]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*6.1*/("""
"""),format.raw/*11.4*/("""
"""),_display_(/*12.2*/main("Kudos Heroes - Home")/*12.29*/(user)/*12.35*/(users)/*12.42*/ {_display_(Seq[Any](format.raw/*12.44*/("""
    """),format.raw/*16.8*/("""
    """),_display_(/*17.6*/welcome(message, style = "scala")),format.raw/*17.39*/("""

""")))}),format.raw/*19.2*/("""
"""))
      }
    }
  }

  def render(message:String,user:Option[User],users:List[User]): play.twirl.api.HtmlFormat.Appendable = apply(message)(user)(users)

  def f:((String) => (Option[User]) => (List[User]) => play.twirl.api.HtmlFormat.Appendable) = (message) => (user) => (users) => apply(message)(user)(users)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Nov 15 17:49:55 EET 2018
                  SOURCE: /home/nicu/workspace/KudosHeroes/app/views/index.scala.html
                  HASH: 7613ffe17874e18b9683ed4e5646b3114311ecf3
                  MATRIX: 842->95|992->152|1020->347|1048->349|1084->376|1099->382|1115->389|1155->391|1187->519|1219->525|1273->558|1306->561
                  LINES: 24->5|29->6|30->11|31->12|31->12|31->12|31->12|31->12|32->16|33->17|33->17|35->19
                  -- GENERATED --
              */
          