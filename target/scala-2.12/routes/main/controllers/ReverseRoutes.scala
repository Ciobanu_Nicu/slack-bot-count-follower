// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/KudosHeroes/conf/routes
// @DATE:Mon Oct 15 16:35:12 EEST 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:11
package controllers {

  // @LINE:26
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:26
    def versioned(file:String): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "public/" + implicitly[play.api.mvc.PathBindable[String]].unbind("file", file))
    }
  
  }

  // @LINE:22
  class ReverseCountController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def count(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "count")
    }
  
  }

  // @LINE:17
  class ReverseUserController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:19
    def findAllUser(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "allUsers")
    }
  
    // @LINE:30
    def findById(id:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "findById/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("id", id)))
    }
  
    // @LINE:28
    def createUser(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "new")
    }
  
    // @LINE:17
    def getUsers(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "getUsers")
    }
  
    // @LINE:32
    def delete(id:String): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("id", id)))
    }
  
    // @LINE:34
    def update(): Call = {
      
      Call("PUT", _prefix)
    }
  
  }

  // @LINE:39
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def index(id:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("id", id)))
    }
  
  }

  // @LINE:24
  class ReverseAsyncController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def message(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "message")
    }
  
  }

  // @LINE:11
  class ReverseMessageController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def findAllMessage(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "getAllMsg")
    }
  
    // @LINE:11
    def writeTaco(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "test")
    }
  
    // @LINE:13
    def getMessages(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "getMessages")
    }
  
  }


}
