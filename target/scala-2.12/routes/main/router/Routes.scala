// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/KudosHeroes/conf/routes
// @DATE:Mon Oct 15 16:35:12 EEST 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:11
  MessageController_0: controllers.MessageController,
  // @LINE:17
  UserController_4: controllers.UserController,
  // @LINE:22
  CountController_1: controllers.CountController,
  // @LINE:24
  AsyncController_3: controllers.AsyncController,
  // @LINE:26
  Assets_5: controllers.Assets,
  // @LINE:39
  HomeController_2: controllers.HomeController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:11
    MessageController_0: controllers.MessageController,
    // @LINE:17
    UserController_4: controllers.UserController,
    // @LINE:22
    CountController_1: controllers.CountController,
    // @LINE:24
    AsyncController_3: controllers.AsyncController,
    // @LINE:26
    Assets_5: controllers.Assets,
    // @LINE:39
    HomeController_2: controllers.HomeController
  ) = this(errorHandler, MessageController_0, UserController_4, CountController_1, AsyncController_3, Assets_5, HomeController_2, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, MessageController_0, UserController_4, CountController_1, AsyncController_3, Assets_5, HomeController_2, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """test""", """controllers.MessageController.writeTaco"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getMessages""", """controllers.MessageController.getMessages"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getAllMsg""", """controllers.MessageController.findAllMessage"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getUsers""", """controllers.UserController.getUsers"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """allUsers""", """controllers.UserController.findAllUser()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """count""", """controllers.CountController.count"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """message""", """controllers.AsyncController.message"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """public/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """new""", """controllers.UserController.createUser"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """findById/""" + "$" + """id<[^/]+>""", """controllers.UserController.findById(id:String)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """id<[^/]+>""", """controllers.UserController.delete(id:String)"""),
    ("""PUT""", this.prefix, """controllers.UserController.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """id<[^/]+>""", """controllers.HomeController.index(id:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:11
  private[this] lazy val controllers_MessageController_writeTaco0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("test")))
  )
  private[this] lazy val controllers_MessageController_writeTaco0_invoker = createInvoker(
    MessageController_0.writeTaco,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MessageController",
      "writeTaco",
      Nil,
      "GET",
      this.prefix + """test""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_MessageController_getMessages1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getMessages")))
  )
  private[this] lazy val controllers_MessageController_getMessages1_invoker = createInvoker(
    MessageController_0.getMessages,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MessageController",
      "getMessages",
      Nil,
      "GET",
      this.prefix + """getMessages""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_MessageController_findAllMessage2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getAllMsg")))
  )
  private[this] lazy val controllers_MessageController_findAllMessage2_invoker = createInvoker(
    MessageController_0.findAllMessage,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MessageController",
      "findAllMessage",
      Nil,
      "GET",
      this.prefix + """getAllMsg""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_UserController_getUsers3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getUsers")))
  )
  private[this] lazy val controllers_UserController_getUsers3_invoker = createInvoker(
    UserController_4.getUsers,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "getUsers",
      Nil,
      "GET",
      this.prefix + """getUsers""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_UserController_findAllUser4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("allUsers")))
  )
  private[this] lazy val controllers_UserController_findAllUser4_invoker = createInvoker(
    UserController_4.findAllUser(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "findAllUser",
      Nil,
      "GET",
      this.prefix + """allUsers""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_CountController_count5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("count")))
  )
  private[this] lazy val controllers_CountController_count5_invoker = createInvoker(
    CountController_1.count,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CountController",
      "count",
      Nil,
      "GET",
      this.prefix + """count""",
      """ An example controller showing how to use dependency injection""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_AsyncController_message6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("message")))
  )
  private[this] lazy val controllers_AsyncController_message6_invoker = createInvoker(
    AsyncController_3.message,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AsyncController",
      "message",
      Nil,
      "GET",
      this.prefix + """message""",
      """ An example controller showing how to write asynchronous code""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_Assets_versioned7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("public/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned7_invoker = createInvoker(
    Assets_5.versioned(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[String]),
      "GET",
      this.prefix + """public/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_UserController_createUser8_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("new")))
  )
  private[this] lazy val controllers_UserController_createUser8_invoker = createInvoker(
    UserController_4.createUser,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "createUser",
      Nil,
      "POST",
      this.prefix + """new""",
      """ Create User (Test)""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_UserController_findById9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("findById/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserController_findById9_invoker = createInvoker(
    UserController_4.findById(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "findById",
      Seq(classOf[String]),
      "GET",
      this.prefix + """findById/""" + "$" + """id<[^/]+>""",
      """ Get User By ID (Test)""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_UserController_delete10_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserController_delete10_invoker = createInvoker(
    UserController_4.delete(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "delete",
      Seq(classOf[String]),
      "DELETE",
      this.prefix + """""" + "$" + """id<[^/]+>""",
      """ Delete User (Test)""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_UserController_update11_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_UserController_update11_invoker = createInvoker(
    UserController_4.update,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "update",
      Nil,
      "PUT",
      this.prefix + """""",
      """ Update user (Test)""",
      Seq()
    )
  )

  // @LINE:39
  private[this] lazy val controllers_HomeController_index12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_index12_invoker = createInvoker(
    HomeController_2.index(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Seq(classOf[String]),
      "GET",
      this.prefix + """""" + "$" + """id<[^/]+>""",
      """ TESTING USER ROUTES""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:11
    case controllers_MessageController_writeTaco0_route(params@_) =>
      call { 
        controllers_MessageController_writeTaco0_invoker.call(MessageController_0.writeTaco)
      }
  
    // @LINE:13
    case controllers_MessageController_getMessages1_route(params@_) =>
      call { 
        controllers_MessageController_getMessages1_invoker.call(MessageController_0.getMessages)
      }
  
    // @LINE:15
    case controllers_MessageController_findAllMessage2_route(params@_) =>
      call { 
        controllers_MessageController_findAllMessage2_invoker.call(MessageController_0.findAllMessage)
      }
  
    // @LINE:17
    case controllers_UserController_getUsers3_route(params@_) =>
      call { 
        controllers_UserController_getUsers3_invoker.call(UserController_4.getUsers)
      }
  
    // @LINE:19
    case controllers_UserController_findAllUser4_route(params@_) =>
      call { 
        controllers_UserController_findAllUser4_invoker.call(UserController_4.findAllUser())
      }
  
    // @LINE:22
    case controllers_CountController_count5_route(params@_) =>
      call { 
        controllers_CountController_count5_invoker.call(CountController_1.count)
      }
  
    // @LINE:24
    case controllers_AsyncController_message6_route(params@_) =>
      call { 
        controllers_AsyncController_message6_invoker.call(AsyncController_3.message)
      }
  
    // @LINE:26
    case controllers_Assets_versioned7_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_versioned7_invoker.call(Assets_5.versioned(path, file))
      }
  
    // @LINE:28
    case controllers_UserController_createUser8_route(params@_) =>
      call { 
        controllers_UserController_createUser8_invoker.call(UserController_4.createUser)
      }
  
    // @LINE:30
    case controllers_UserController_findById9_route(params@_) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_UserController_findById9_invoker.call(UserController_4.findById(id))
      }
  
    // @LINE:32
    case controllers_UserController_delete10_route(params@_) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_UserController_delete10_invoker.call(UserController_4.delete(id))
      }
  
    // @LINE:34
    case controllers_UserController_update11_route(params@_) =>
      call { 
        controllers_UserController_update11_invoker.call(UserController_4.update)
      }
  
    // @LINE:39
    case controllers_HomeController_index12_route(params@_) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_HomeController_index12_invoker.call(HomeController_2.index(id))
      }
  }
}
