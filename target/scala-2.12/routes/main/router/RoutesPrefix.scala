// @GENERATOR:play-routes-compiler
// @SOURCE:/home/nicu/workspace/KudosHeroes/conf/routes
// @DATE:Mon Oct 15 16:35:12 EEST 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
