package models

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Taco(id: String, tacos: Int, ts: String)


object Taco{

  implicit def optionFormat[T: Format]: Format[Option[T]] = new Format[Option[T]]{
    override def reads(json: JsValue): JsResult[Option[T]] = json.validateOpt[T]

    override def writes(o: Option[T]): JsValue = o match {
      case Some(t) ⇒ implicitly[Writes[T]].writes(t)
      case None ⇒ JsNull
    }
  }

  implicit val tacoReads: Reads[Taco] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "tacos").read[Int] and
      (JsPath \ "ts").read[String]
    ) (Taco.apply _)

  implicit val tacoWrites: OWrites[Taco] = (
    (JsPath \ "id").write[String] and
      (JsPath \ "tacos").write[Int] and
      (JsPath \ "ts").write[String]
    ) (unlift(Taco.unapply))

  implicit val tacosFormat: OFormat[Taco] = Json.format[Taco]

  implicit val tcFormat: Format[Taco] = Format(tacoReads, tacoWrites)

  }

