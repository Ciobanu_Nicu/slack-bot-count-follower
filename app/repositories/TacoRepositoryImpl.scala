package repositories

import javax.inject.Inject
import models.Taco
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoApi
import play.modules.reactivemongo.json._
import reactivemongo.api.{Cursor, FailoverStrategy, ReadPreference}
import reactivemongo.play.json.collection.JSONCollection

import scala.concurrent.{ExecutionContext, Future}

class TacoRepositoryImpl @Inject()(reactiveMongoApi: ReactiveMongoApi) (implicit ec: ExecutionContext) extends TacoRepository {

  def collection: Future[JSONCollection] = {
    reactiveMongoApi.database.map(_.collection("taco", FailoverStrategy.default))
  }

  def create(taco: Taco) = {
    val newID = taco.id
    val r = taco.copy(
      id = newID
    )
    val newMessage = for {
      _ <- collection.flatMap(_.insert[Taco](r))
    } yield Some(r)
    newMessage
  }

  override def findById(id: String): Future[Option[Taco]] = {
    collection.flatMap(_.find(Json.obj("id" -> id)).one[Taco](ReadPreference.primary))
  }

  override def findByIdTs(id: String, ts: String): Future[Option[Taco]] = {
    collection.flatMap(_.find(Json.obj("id" -> id, "ts" -> ts)).one[Taco](ReadPreference.primary))
  }

  override def findByTs(ts: String): Future[Option[Taco]] = {
    collection.flatMap(_.find(Json.obj("ts" -> ts)).one[Taco](ReadPreference.primary))
  }

  override def update(taco: Taco) = {
    val s = Json.obj("id" -> taco.id)
    val msg = taco
    for{
      _ <- collection
        .flatMap(_.update(s, msg))
      r <- findById(msg.id)
    } yield r
  }

}
