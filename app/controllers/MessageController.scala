package controllers

import akka.actor.ActorSystem
import javax.inject.{Inject, Singleton}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, ControllerComponents}
import services.{MessageService, TacoService, UserService}
import models.{Message, Taco, UserMessage}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import slack.api.SlackApiClient

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class MessageController @Inject()
(cc: ControllerComponents,tacoService: TacoService, messageService: MessageService, ws: WSClient, userService: UserService)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def getTime(ts: Double) = {

    val dt = new DateTime(ts.toLong * 1000)
    dt

  }

  def getDate(ts: Long) = {

    val dtFormatter = DateTimeFormat.forPattern("dd-MM-yyyy")
    val res = dtFormatter.print(ts * 1000)
    res

  }

  def countWords(text: String) = {
    val counts = mutable.Map.empty[String, Int].withDefaultValue(0)
    for (rawWord <- text.split("[:,!.]+")) {
      val word = rawWord
      counts(word) += 1
    }
    counts
  }


 def extractUserMsgList(message: models.UserMessage) = {

   val reg = "[:A-z0-9]+".r
   val list = reg.findAllIn(message.text).toList

   val listId = for(users <- userService.findByListId(list)) yield users.map(_.id)
   Await.result(listId, Duration(1, "seconds"))
 }

  def extractTaco(message: models.UserMessage) = {

    val reg = "[:A-z0-9]+".r
    val list = reg.findAllIn(message.text).toList
    val tacos = countWords(list.toString()).filter(_._1.equals("taco"))
    val counter = tacos.get("taco").getOrElse(0)

    counter
  }

/*  def getDayOfTaco = Action { implicit request =>

    val listOfAllMessages = messageService.findAll()

    val usersMessages = for{

      msgs <- listOfAllMessages
    } yield {
      val listUsrMsg = msgs.groupBy(_.sender)

      val listUsrMsgs = listUsrMsg.map{list =>
        val map = list._2.map{msg =>
          (getTime(msg.ts.toDouble).dayOfYear().get, msg)
        }.groupBy(_._1).map{m =>
          (m._1, m._2.map(_._2))
        }
        (list._1, map)
      }.toList

      listUsrMsgs

    }

    for{
      msgList <- usersMessages.map(x => x.map(c => c._2))
    } yield{
            val msg = msgList.map(m => m.get(_))

    }

    println("Messages: ========================== ")

    for(msg <- usersMessages) msg.map(usr => println(usr))

    Ok("Oki Doki!")

  }*/

  //Testing get message by sender id and ts


  def getTaco(id: String,ts: String) = {

    val userMessage = messageService.findBySenderIdTs(id,ts)

    val s = for(smsg <- userMessage) yield smsg.map(r => r.tacos).seq.reduceLeft(_ + _)

    var validMessages = new ListBuffer[Message]()

    var tacos = 0

    //list of messages with true number of taco
    val listValMess =
      for{
        messages <- userMessage
      } yield {

        messages.map { msg =>
          if (tacos + msg.tacos <= 5) {
            tacos = tacos + msg.tacos
            validMessages += msg
          } else
            None
        }
        validMessages
      }


    val receiversMessage = for{
      r <- listValMess
    } yield r.groupBy(_.receiver)


    val result = for{
      rcv <- receiversMessage
    } yield rcv.map { tc =>

      val time = tc._2.groupBy(_.ts).toList
      val er = tc._2.map{ t =>
        t.tacos
      }.sum

      (tc._1, er, time.map(t => t._1).head)
    }.toList


    for{
      usrs <- result
    } yield usrs.map{usr =>

      for{
        u <- usr._1
      } yield {

        val exist = Await.result(tacoService.findByIdTs(u, usr._3), Duration(1,"seconds"))
        val value = exist.get

        println("Exist: " + exist)

        exist match {
          case Some(ut) => {tacoService.update(Taco(ut.id, exist.get.tacos + usr._2, ut.ts))}
          case None => tacoService.create(Taco(u, usr._2, usr._3))
        }

      }
    }

    for{
      res <- result
    } res.map(r => println("Result: " + r))

    Ok("Ok")


  }



  def writeTaco = Action { implicit request =>

    val messages = Await.result(messageService.findAll(), Duration(1, "seconds"))
    for{
      msgs <- messages
    } getTaco(msgs.sender, msgs.ts)

    Ok("Good!")

  }


  def getTest = Action { implicit request =>

//    val msgs = Await.result(messageService.findAll(), Duration(1, "seconds"))

    val userMessage = messageService.findBySenderIdTs("UCTFR4RC2","15-10-2018")

    val s = for(smsg <- userMessage) yield smsg.map(r => r.tacos).seq.reduceLeft(_ + _)

    var validMessages = new ListBuffer[Message]()

    var tacos = 0

    val listValMess =
      for{
        messages <- userMessage
      } yield {

        messages.map { msg =>
          if (tacos + msg.tacos <= 5) {
            tacos = tacos + msg.tacos
            validMessages += msg
          } else
            None
        }
        validMessages
      }

    userMessage.map(m => println(m))

    println("//================================//")

    listValMess.map(msg => println("Valid Messages: " + msg))

    val receiversMessage = for{
      r <- listValMess
    } yield r.groupBy(_.receiver)

    val result = for{
      rcv <- receiversMessage
    } yield rcv.map { tc =>

      val time = tc._2.groupBy(_.ts).toList
    val er = tc._2.map{ t =>
      t.tacos
    }.sum

      (tc._1, er, time.map(t => t._1).head)
    }.toList

    for{
      usrs <- result
      } yield usrs.map{usr =>

      for{
        u <- usr._1
      } yield {

        val exist = Await.result(tacoService.findByIdTs(u, usr._3), Duration(1,"seconds"))

        if(exist.isEmpty){
          tacoService.create(Taco(u, usr._2, usr._3))
        }else{
            tacoService.update(Taco(exist.get.id, exist.get.tacos + usr._2, exist.get.ts))
        }
      }
    }


    for{
      res <- result
    } res.map(r => println("Result: " + r))

    Ok("Ok")


  }

  //Put in database all messages from channel who have <<taco>>

  def getMessages = Action { implicit request =>

    implicit val system = ActorSystem("slack")
    implicit val ec = system.dispatcher
    val token = "xoxa-2-435422739219-440042584230-438104368577-f02e4038043c178be6e78f5a7c8987f4"
    val client = SlackApiClient(token)

    val message = Await.result(client.getChannelHistory("CCV15TEKY", None, None, None, None), Duration.Inf)
    println("Messages: " + message)


    val messages = for (
      msg <- client.getChannelHistory("CCV15TEKY", None, None, None, None)
    ) yield{
      msg.messages
    }

    implicit val msgListJson = Json.format[UserMessage]

    val futureMsgs = for(m <- messages) yield {
      m.flatMap(temp => temp.validate[UserMessage] match {
        case JsSuccess(result, _) => Some(result)
        case JsError(err) =>
          //println("ce am vrut sa vedem: " + temp)
          None
      })
    }



    val finalMessages = for (
      msgs <- futureMsgs
    ) yield {
      msgs.map(msg =>
        Message(msg.client_msg_id,
          msg.user,
          extractUserMsgList(msg),
          msg.text,
          extractTaco(msg),
          getDate(msg.ts.toDouble.toLong)))
    }

    for(
      mess <- finalMessages
    ) mess.map{ms =>
      val existMsg = Await.result(messageService.findById(ms.id), Duration.Inf)

      if(ms.receiver.length * ms.tacos <= 5 && ms.receiver.length * ms.tacos  >= 1 && !ms.text.contains("USLACKBOT") && !ms.receiver.contains(ms.sender) && existMsg.isEmpty)
        messageService.create(ms)
      else
        None
    }

    Ok("Ok")
  }



  def createMessage() = Action.async(parse.json) { implicit request =>
    request.body.validate[Message].fold(
      _ => Future.successful(BadRequest(Json.obj("status" -> "Invalid"))),
      message => {
        messageService
          .create(message)
          .map(Json.toJson(_))
          .map(Created(_))
      }
    )
  }

  def findById(id: String) = Action.async { implicit request =>
    messageService
      .findById(id)
      .map(Json.toJson(_))
      .map(Ok(_))

  }

  def findBySenderId(sender: String, ts: String) = Action.async { implicit request =>
    messageService
      .findBySenderIdTs(sender, ts)
      .map(Json.toJson(_))
      .map(Ok(_))
  }


  def findAllMessage() = Action.async { implicit request =>
    messageService
      .findAll()
      .map(Json.toJson(_))
      .map(Ok(_))
  }

  def findByName(name: String) = Action.async { implicit request =>
    messageService
      .findByName(name)
      .map(Json.toJson(_))
      .map(Ok(_))
  }

  def delete(id: String) = Action.async { implicit request =>
    messageService
      .delete(id)
      .map(_ => Ok)
  }

  def update() = Action.async(parse.json) { implicit request =>
    request.body.validate[Message].fold(
      _ => Future.successful(BadRequest(Json.obj("status" -> "Invalid"))),
      message => {
        messageService
          .update(message)
          .map(Json.toJson(_))
          .map(Ok(_))
      }
    )
  }
}