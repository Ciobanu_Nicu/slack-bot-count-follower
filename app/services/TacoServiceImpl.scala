package services

import models.Taco
import javax.inject.Inject
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.FailoverStrategy
import reactivemongo.play.json.collection.JSONCollection
import repositories.TacoRepository

import scala.concurrent.{ExecutionContext, Future}

class TacoServiceImpl @Inject()(reactiveMongoApi: ReactiveMongoApi, tacoRepository: TacoRepository)(implicit ex: ExecutionContext) extends TacoService {

  def collection: Future[JSONCollection] = {
    reactiveMongoApi.database.map(_.collection("taco", FailoverStrategy.default))
  }

  def create(taco: Taco) = {
    tacoRepository.create(taco)
  }

  override def findById(id: String): Future[Option[Taco]] = {
    tacoRepository.findById(id)
  }

  override def findByIdTs(id: String, ts: String): Future[Option[Taco]] = {
    tacoRepository.findByIdTs(id, ts)
  }

  override def findByTs(ts: String): Future[Option[Taco]] = {
    tacoRepository.findById(ts)
  }

  override def update(taco: Taco) = {
    tacoRepository.update(taco)
  }

}
