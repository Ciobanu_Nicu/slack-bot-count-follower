package services

import models.Taco

import scala.concurrent.Future

trait TacoService {

  def create(taco: Taco): Future[Option[Taco]]

  def findById(id: String): Future[Option[Taco]]

  def findByIdTs(id: String, ts: String): Future[Option[Taco]]

  def findByTs(ts: String): Future[Option[Taco]]

  def update(taco: Taco): Future[Option[Taco]]

}
